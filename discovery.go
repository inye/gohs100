package main

import (
	"net"
	"strings"
	"time"
)

func PerformDiscovery() (devices map[string]string, err error) {
	// Open UDP socket and set timeout
	LocalAddr, err := net.ResolveUDPAddr("udp", ":0")
	if err != nil {
		return
	}

	Listener, err := net.ListenUDP("udp", LocalAddr)
	if err != nil {
		return
	}
	defer Listener.Close()
	err = Listener.SetReadDeadline(time.Now().Add(3 * time.Second))
	if err != nil {
		return
	}

	// Send broadcast message
	BroadcastAddr, err := net.ResolveUDPAddr("udp", "255.255.255.255:9999")
	if err != nil {
		return
	}

	request := `{"system": {"get_sysinfo": null}, "emeter": {"get_realtime": null}}`
	ciphertext, err := ProtocolEncrypt(request)
	if err != nil {
		return
	}

	_, err = Listener.WriteTo(ciphertext[4:], BroadcastAddr)
	if err != nil {
		return
	}

	// Read answers
	devices = make(map[string]string, 0)
	rcv_buf := make([]byte, 4096)
	for {
		var addr net.Addr
		_, addr, err = Listener.ReadFrom(rcv_buf)
		if e, ok := err.(net.Error); ok && e.Timeout() {
			err = nil
			return
		} else if err != nil {
			return
		}
		if err == nil {
			plain, _ := ProtocolDecrypt(rcv_buf)
			devices[strings.Split(addr.String(), ":")[0]] = plain
		}
	}
}
