package main

import "os"
import "fmt"

func usage() {
	binary := os.Args[0]
	fmt.Printf("Usage: %s discover # attempts to find all smartplugs in local network\n", binary)
	fmt.Printf("       %s get_state <smart plug host or ip> # shows smartplug state\n", binary)
	fmt.Printf("       %s turn_on <smart plug host or ip> # turns smartplug ON\n", binary)
	fmt.Printf("       %s turn_off <smart plug host or ip> # turns smartplug OFF\n", binary)
	os.Exit(1)
}

func main() {
	if len(os.Args) < 2 {
		usage()
	}

	var command string

	// Perform discovery and exit. Perform command otherwise.
	switch os.Args[1] {
	case "discover":
		devices, err := PerformDiscovery()
		if err != nil {
			fmt.Printf("Failed to perform discovery: %s\n", err)
			os.Exit(2)
		}
		for addr, info := range devices {
			fmt.Printf("%s %s\n", addr, info)
		}
		os.Exit(0)
	case "turn_on":
		command = `{"system": {"set_relay_state": {"state": 1}}}`
	case "turn_off":
		command = `{"system": {"set_relay_state": {"state": 0}}}`
	case "get_state":
		command = `{"system": {"get_sysinfo": null}}`
	default:
		usage()
	}

	if len(os.Args) < 3 {
		usage()
	}
	rv, err := Query(os.Args[2], command)
	if err != nil {
		fmt.Printf("Failed to perform command %s: %s\n", os.Args[1], err)
		os.Exit(3)
	}
	fmt.Println(rv)

	os.Exit(0)

}
