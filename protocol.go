package main

import (
	"bytes"
	"encoding/binary"
	"fmt"
	"net"
)

const initializationVector byte = 171

func ProtocolEncrypt(request string) (rv []byte, err error) {
	key := initializationVector
	ciphertext := []byte(request)
	for idx, _ := range ciphertext {
		ciphertext[idx] = ciphertext[idx] ^ key
		key = ciphertext[idx]
	}
	buf := new(bytes.Buffer)

	err = binary.Write(buf, binary.BigEndian, uint32(len(ciphertext)))
	if err != nil {
		return
	}
	_, err = buf.Write(ciphertext)
	if err != nil {
		return
	}
	rv = buf.Bytes()
	return
}

func ProtocolDecrypt(ciphertext []byte) (rv string, err error) {
	plaintext := make([]byte, len(ciphertext))
	key := initializationVector
	for idx, _ := range plaintext {
		plaintext[idx] = ciphertext[idx] ^ key
		key = ciphertext[idx]
	}
	rv = string(plaintext)
	return
}

func Query(host string, request string) (rv string, err error) {
	conn, err := net.Dial("tcp", fmt.Sprintf("%s:9999", host))
	if err != nil {
		return
	}
	defer conn.Close()

	cipher, err := ProtocolEncrypt(request)
	if err != nil {
		return
	}

	_, err = conn.Write(cipher)
	if err != nil {
		return
	}

	length := -1
	buf := new(bytes.Buffer)
	rcv_buf := make([]byte, 4096)
	for {
		_, err = conn.Read(rcv_buf)
		if err != nil {
			return
		}
		if length == -1 {
			length = int(binary.BigEndian.Uint32(rcv_buf[0:]))
		}
		buf.Write(rcv_buf)
		if length > 0 && buf.Len() >= length+4 {
			break
		}
	}

	rv, err = ProtocolDecrypt(buf.Bytes()[4:])
	return
}
