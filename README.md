gohs100
=======

Command-line tool to control [TP-Link HS
100](https://www.tp-link.com/us/products/details/cat-5516_HS100.html)/[TP-Link HS
110](https://www.tp-link.com/us/products/details/cat-5516_HS110.html).

I want to control my HS110 from ASUS router SSH command-line. I don't want to be bothered with
installation of complete python environment, hence i couldn't use excellent
[pyHS100](https://github.com/GadgetReactor/pyHS100) project. However, i could make crude port of
bits of pyHS100 to golang and cross-compile nice static binary. Also that's great excuse to play
with go first time for me.

Usage
-----

Currently gohs100 performs no parsing of responses and does not checks for device errors. Also,
only essential operations are supported.

* `gohs100 discover`: attempts to discover smartplugs in local network via broadcast query. Waits
  for 3 seconds for answers and prints them in raw form along with ip addresses.
* `gohs100 get_state <smartplug ip>`: queries status from smartplug
* `gohs100 turn_on <smartplug ip>`: turn smarplug on
* `gohs100 turn_off <smartplug ip>`: turn smarplug off



